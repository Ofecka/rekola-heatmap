Rekola heatmap
===========================

How to run
-----------------

- Create database a modify `index.php` (change username, password, host, db name)
- Get data from somewhere and populate the db
- Install `mango-cli` (`npm install -g mango-cli`)
- Run `mango install && mango build`
- Navigate to the `index.php` in your browser
