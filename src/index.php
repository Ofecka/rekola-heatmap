<?php
$dateRange = [
	'from' => '2016-01-01',
	'to' => '2016-05-20',
];
$intensity = 0.3;
$radius = 20;

if (isset($_GET['from'])) {
	$dateRange['from'] = (string)$_GET['from'];
}
if (isset($_GET['to'])) {
	$dateRange['to'] = (string)$_GET['to'];
}
if (isset($_GET['radius'])) {
	$radius = (string)$_GET['radius'];
}
if (isset($_GET['intensity'])) {
	$intensity = (string)$_GET['intensity'];
}

$locations = [];

$conn = new mysqli('localhost', 'root', '', 'rekola-heatmap');
if ($conn->connect_error) {
	die('Connection failed: ' . $conn->connect_error);
}

$sql = 'SELECT latitude, longitude FROM `data` WHERE `time` > "'.
	$conn->real_escape_string($dateRange['from'])
	.'" AND `time` < "'.
	$conn->real_escape_string($dateRange['to'])
	.'"';

$result = $conn->query($sql);

if ($result) {
	while ($row = $result->fetch_array()) {
		$locations[] = [
			'lat' => $row['latitude'],
			'lng' => $row['longitude']
		];
	}
}

?><!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Rekola heatmap</title>
	<link rel="stylesheet" href="styles/index.css">
</head>
<body>
	<form class="form" action="." method="get">
		<label class="form-label" for="date-from">From:</label>
		<input class="form-input" type="date" name="from" id="date-from" value="<?php echo $dateRange['from']; ?>">

		<label class="form-label" for="date-to">To:</label>
		<input class="form-input" type="date" name="to" id="date-to" value="<?php echo $dateRange['to']; ?>">

		<label class="form-label" for="intensity">Intensity:</label>
		<input class="form-input" type="number" min="0" max="1" step="0.1" name="intensity" id="intensity" value="<?php echo $intensity; ?>">

		<label class="form-label" for="radius">Radius:</label>
		<input class="form-input" type="number" min="0" max="100" step="1" name="radius" id="radius" value="<?php echo $radius; ?>">

		<input class="form-submit" type="submit" value="Show">
	</form>

	<div class="map" id="map"></div>

	<script>
		window.mapData = {
			position: {
				lat: 50.0755,
				lng: 14.4378
			},
			locations: <?php echo json_encode($locations); ?>,
			intensity: <?php echo $intensity; ?>,
			radius: <?php echo $radius; ?>
		}
	</script>

	<script src="scripts/index.js"></script>
</body>
</html>
