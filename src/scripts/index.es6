var L = require('leaflet')
require('leaflet.heat')



var map = L.map('map').setView([
	window.mapData.position.lat,
	window.mapData.position.lng],
	12
)

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicmVrb2xhIiwiYSI6ImNpbzRwOTg0ZTAwMjB2eWx5NGFmdDQ1c3UifQ.gAnJySaTv6pSmTJoAIkgIA', {
	maxZoom: 19,
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
		'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="http://mapbox.com">Mapbox</a>',
	id: 'mapbox.streets'
}).addTo(map);

var locations = []
window.mapData.locations.forEach(function (location) {
	locations.push([
		location.lat,
		location.lng,
		window.mapData.intensity
	])
})
var heat = L.heatLayer(
	locations,
	{
		radius: window.mapData.radius
	}
).addTo(map)
